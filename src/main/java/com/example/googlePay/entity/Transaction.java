package com.example.googlePay.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Table(name = "transaction")
@Entity
@Getter
@Setter
public class Transaction implements Serializable {

	private static final long serialVersionUID = 1986664567979L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trans_id")
	private long id;

	@Column(name = "from_phone")
	private long fromPhone;

	@Column(name = "to_phone")
	private long toPhone;

	@Column(name = "amount")
	private float amount;

	@Column(name = "status")
	private String status;

	@CreationTimestamp
	@Column(name = "create_date")
	private LocalDateTime createDate;

	@UpdateTimestamp
	@Column(name = "modify_date")
	private LocalDateTime modifyDate;

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", fromPhone=" + fromPhone + ", toPhone=" + toPhone + ", amount=" + amount
				+ ", status=" + status + ", createDate=" + createDate + ", modifyDate=" + modifyDate + "]";
	}

}
