package com.example.googlePay.entity.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GooglePayRequestDto {

	@NotNull(message = "Name can't be empty")
	private String fname;

	private String lname;

	@Min(18)
	@Max(99)
	private Integer age;

	@NotNull(message = "can not be null")
	private long phoneNumber;

}
