package com.example.googlePay.entity.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GooglePayTransactionsResponseDto {

	private int statusCode;
	private String statusMessage;

	List<GooglePayTransactionResponseDto> googlePayTransactionResponseDtos = new ArrayList<>();

}
