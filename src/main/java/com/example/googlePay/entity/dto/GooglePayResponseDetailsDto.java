package com.example.googlePay.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GooglePayResponseDetailsDto {

	private int statusCode;
	private String statusMessage;

	private GooglePayResponseDto googlePayResponseDto;
}
