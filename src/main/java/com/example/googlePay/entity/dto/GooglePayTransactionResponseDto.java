package com.example.googlePay.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GooglePayTransactionResponseDto {

	private long id;
	private long fromPhone;
	private long toPhone;
	private float amount;
	private String status;


}
