package com.example.googlePay.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GooglePayResponseDto {

	private long id;

	private String fname;

	private String lname;

	private Integer age;

	private long phoneNumber;
}
