package com.example.googlePay.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GooglePayTransactionRequestDto {

	private long fromPhone;
	private long toPhone;

}
