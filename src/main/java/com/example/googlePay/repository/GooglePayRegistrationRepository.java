package com.example.googlePay.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.googlePay.entity.GooglePay;

@Repository
public interface GooglePayRegistrationRepository extends JpaRepository<GooglePay, Long> {

	Optional<GooglePay> findByPhoneNumber(long phoneNumber);

}
