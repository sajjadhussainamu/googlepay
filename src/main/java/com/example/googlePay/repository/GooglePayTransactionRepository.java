package com.example.googlePay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.googlePay.entity.Transaction;

@Repository
public interface GooglePayTransactionRepository extends JpaRepository<Transaction, Long> {

	List<Transaction> findTop10ByFromPhoneOrToPhoneOrderByCreateDateDesc(long phoneNumber,long toPhone);

}
