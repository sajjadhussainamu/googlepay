package com.example.googlePay.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.googlePay.entity.GooglePay;
import com.example.googlePay.entity.dto.GooglePayTransactionsResponseDto;
import com.example.googlePay.service.GooglePayRegistrationService;
import com.example.googlePay.service.GooglePayTransactionService;

@RestController
@RequestMapping("/googlepay_transactions")
public class GooglePayTransactionController {

	@Autowired
	GooglePayTransactionService googlePayTransactionService;

	@Autowired
	GooglePayRegistrationService googlePayRegistrationService;

	@PostMapping("/fund_transafer_by_phone_number")
	public GooglePayTransactionsResponseDto process(@RequestParam long fromPhone, @RequestParam long toPhone,
			@RequestParam float amount) {
		Optional<GooglePay> fromPhoneResponseDto = googlePayRegistrationService.getByPhoneNumber(fromPhone);
		Optional<GooglePay> toPhoneResponseDto = googlePayRegistrationService.getByPhoneNumber(toPhone);

		GooglePayTransactionsResponseDto googlePayResponseDto = new GooglePayTransactionsResponseDto();
		if (!fromPhoneResponseDto.isPresent() || !toPhoneResponseDto.isPresent()) {
			googlePayResponseDto.setStatusCode(400);
			googlePayResponseDto.setStatusMessage("phone is not Registered Please registred  phone number");
		} else {
			googlePayResponseDto = googlePayTransactionService.fundTransfer(fromPhone, toPhone, amount);
		}

		return googlePayResponseDto;
	}

}
