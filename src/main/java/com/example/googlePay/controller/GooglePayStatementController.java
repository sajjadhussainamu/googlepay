package com.example.googlePay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.googlePay.entity.dto.GooglePayTransactionsResponseDto;
import com.example.googlePay.service.GooglePayTransactionService;

@RestController
@RequestMapping("/google_pay_statements")
public class GooglePayStatementController {

	@Autowired
	GooglePayTransactionService googlePayTransactionService;

	@GetMapping("latest10Transactions")
	public GooglePayTransactionsResponseDto latestTransactions(@RequestParam long phoneNumber) {

	GooglePayTransactionsResponseDto responseDto = googlePayTransactionService.latestTransactions(phoneNumber);
		return responseDto;
	}

}
