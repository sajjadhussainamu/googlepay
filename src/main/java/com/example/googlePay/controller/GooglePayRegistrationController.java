package com.example.googlePay.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.googlePay.entity.GooglePay;
import com.example.googlePay.entity.dto.GooglePayRequestDto;
import com.example.googlePay.entity.dto.GooglePayResponseDetailsDto;
import com.example.googlePay.entity.dto.GooglePayResponseDto;
import com.example.googlePay.service.GooglePayRegistrationService;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/registration")
public class GooglePayRegistrationController {

	@Autowired
	GooglePayRegistrationService googlePayRegistrationService;

	@PostMapping(value = "/googlepay")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully Registered"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })

	public GooglePayResponseDetailsDto process(@Valid @RequestBody GooglePayRequestDto googlePayRequestDto) {
		GooglePayResponseDetailsDto payResponseDetailsDto = new GooglePayResponseDetailsDto();
		int length = (int) (Math.log10(googlePayRequestDto.getPhoneNumber()) + 1);
		if (length != 10) {
			payResponseDetailsDto.setStatusCode(400);
			payResponseDetailsDto.setStatusMessage("Phone Number should be 10 Digits");
			return payResponseDetailsDto;
		}
		Optional<GooglePay> PayResponseDto = googlePayRegistrationService
				.getByPhoneNumber(googlePayRequestDto.getPhoneNumber());
		if (PayResponseDto.isPresent()) {
			payResponseDetailsDto.setStatusCode(400);
			payResponseDetailsDto.setStatusMessage("Phone Number Already Registered Please change you number");
			return payResponseDetailsDto;
		}

		GooglePayResponseDto googlePayResponseDto = googlePayRegistrationService.registration(googlePayRequestDto);

		payResponseDetailsDto.setStatusCode(200);
		payResponseDetailsDto.setStatusMessage("Registration Success");
		payResponseDetailsDto.setGooglePayResponseDto(googlePayResponseDto);
		return payResponseDetailsDto;
	}
}
