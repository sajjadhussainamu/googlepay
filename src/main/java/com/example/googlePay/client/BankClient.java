package com.example.googlePay.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.googlePay.entity.dto.GooglePayTransactionsResponseDto;

@FeignClient(name = "http://BANK-SERVICE")
public interface BankClient {

	@GetMapping("/wawa/registraions/getByPhoneNumber")
	public ResponseEntity<Integer> getByPhoneNumber(@RequestParam("phoneNumber") long phoneNumber);

	@PostMapping("/wawa/transactions/byMobileNumber")
	public GooglePayTransactionsResponseDto byMobileNumber(@RequestParam("fromNumber") long fromNumber,
			@RequestParam("toNumber") long toNumber, @RequestParam("amount") float amount);
	
	

}
