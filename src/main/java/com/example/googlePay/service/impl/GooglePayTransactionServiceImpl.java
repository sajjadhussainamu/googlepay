package com.example.googlePay.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.googlePay.client.BankClient;
import com.example.googlePay.entity.Transaction;
import com.example.googlePay.entity.dto.GooglePayTransactionResponseDto;
import com.example.googlePay.entity.dto.GooglePayTransactionsResponseDto;
import com.example.googlePay.repository.GooglePayTransactionRepository;
import com.example.googlePay.service.GooglePayTransactionService;

@Service
@Transactional
public class GooglePayTransactionServiceImpl implements GooglePayTransactionService {

	@Autowired
	BankClient bankClient;

	@Autowired
	GooglePayTransactionRepository googlePayTransactionRepository;

	@Override
	public GooglePayTransactionsResponseDto fundTransfer(long fromPhone, long toPhone, float amount) {

		Transaction transaction = new Transaction();
		GooglePayTransactionsResponseDto dtGooglePayTransactionsResponseDto = new GooglePayTransactionsResponseDto();
		GooglePayTransactionsResponseDto responseDto = bankClient.byMobileNumber(fromPhone, toPhone, amount);
		if (responseDto.getStatusCode() == 200) {
			transaction.setStatus("SUCCESS");
		} else {
			transaction.setStatus("FAILED");
		}
		transaction.setAmount(amount);
		transaction.setFromPhone(fromPhone);
		transaction.setToPhone(toPhone);
		googlePayTransactionRepository.save(transaction);
		GooglePayTransactionResponseDto transactionResponseDto = new GooglePayTransactionResponseDto();

		BeanUtils.copyProperties(transaction, transactionResponseDto);

		List<GooglePayTransactionResponseDto> transactionResponseDtos = new ArrayList<>();

		transactionResponseDtos.add(transactionResponseDto);
		dtGooglePayTransactionsResponseDto.setStatusCode(responseDto.getStatusCode());
		dtGooglePayTransactionsResponseDto.setStatusMessage(responseDto.getStatusMessage());
		dtGooglePayTransactionsResponseDto.setGooglePayTransactionResponseDtos(transactionResponseDtos);
		return dtGooglePayTransactionsResponseDto;
	}

	@Override
	public GooglePayTransactionsResponseDto latestTransactions(long phoneNumber) {
		long toPhone = phoneNumber;

		List<Transaction> transactionsList = googlePayTransactionRepository
				.findTop10ByFromPhoneOrToPhoneOrderByCreateDateDesc(phoneNumber, toPhone);

		GooglePayTransactionsResponseDto dtGooglePayTransactionsResponseDto = new GooglePayTransactionsResponseDto();

		List<GooglePayTransactionResponseDto> transactionResponseDtos = new ArrayList<>();
		for (Transaction transaction : transactionsList) {
			GooglePayTransactionResponseDto transactionResponseDto = new GooglePayTransactionResponseDto();
			BeanUtils.copyProperties(transaction, transactionResponseDto);
			transactionResponseDtos.add(transactionResponseDto);
		}

		dtGooglePayTransactionsResponseDto.setGooglePayTransactionResponseDtos(transactionResponseDtos);

		return dtGooglePayTransactionsResponseDto;
	}

}
