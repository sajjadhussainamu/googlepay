package com.example.googlePay.service.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.googlePay.client.BankClient;
import com.example.googlePay.entity.GooglePay;
import com.example.googlePay.entity.dto.GooglePayRequestDto;
import com.example.googlePay.entity.dto.GooglePayResponseDto;
import com.example.googlePay.repository.GooglePayRegistrationRepository;
import com.example.googlePay.service.GooglePayRegistrationService;

@Service
public class GooglePayRegistrationServiceImpl implements GooglePayRegistrationService {

	@Autowired
	GooglePayRegistrationRepository googlePayRegistrationRepository;

	@Autowired
	BankClient bankClient;

	@Override
	public GooglePayResponseDto registration(GooglePayRequestDto googlePayRequestDto) {
		GooglePay googlePay = new GooglePay();
		GooglePayResponseDto googlePayResponseDto = new GooglePayResponseDto();
		BeanUtils.copyProperties(googlePayRequestDto, googlePay);
		ResponseEntity<Integer> responseEntity = bankClient.getByPhoneNumber(googlePay.getPhoneNumber());

		if (responseEntity.getStatusCodeValue() == 200) {
			googlePayRegistrationRepository.save(googlePay);
		}
		BeanUtils.copyProperties(googlePay, googlePayResponseDto);
		return googlePayResponseDto;
	}

	@Override
	public Optional<GooglePay> getByPhoneNumber(long phoneNumber) {
		return googlePayRegistrationRepository.findByPhoneNumber(phoneNumber);
	}

}
