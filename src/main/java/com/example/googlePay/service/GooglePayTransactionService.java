package com.example.googlePay.service;

import com.example.googlePay.entity.dto.GooglePayTransactionsResponseDto;

public interface GooglePayTransactionService {


	GooglePayTransactionsResponseDto fundTransfer(long fromPhone, long toPhone, float amount);

	GooglePayTransactionsResponseDto latestTransactions(long phoneNumber);

}
