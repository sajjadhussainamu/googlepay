package com.example.googlePay.service;

import java.util.Optional;

import com.example.googlePay.entity.GooglePay;
import com.example.googlePay.entity.dto.GooglePayRequestDto;
import com.example.googlePay.entity.dto.GooglePayResponseDto;


public interface GooglePayRegistrationService {

	GooglePayResponseDto registration(GooglePayRequestDto googlePayRequestDto);

	Optional<GooglePay> getByPhoneNumber(long phoneNumber);

}
